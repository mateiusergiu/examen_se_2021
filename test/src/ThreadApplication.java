
/**
 * @author Mateiu Sergiu
 * @version 1
 */

public class ThreadApplication extends Thread {
    ThreadApplication(String name) { // The constructor of the Thread Class where I give the name of the thread
        super(name);
    }

    @Override
    public void run() { // Override the function run so it will do my tasks as I want
        for (int i = 1; i <= 10; i++) {
            System.out.println(getName()+" - "+ i); // getName() takes the name of the current thread
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(currentThread().getName() + " job finalised.");
    }

    public static void main(String[] args) {
        // Creating the threads and starting them
        ThreadApplication myThread1 = new ThreadApplication("MyThread1");
        ThreadApplication myThread2 = new ThreadApplication("MyThread2");
        ThreadApplication myThread3 = new ThreadApplication("MyThread3");

        myThread1.start();
        myThread2.start();
        myThread3.start();
    }
}
