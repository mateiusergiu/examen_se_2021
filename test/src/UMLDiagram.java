
/**
 * @author Mateiu Sergiu
 * @version 1
 */

import java.util.ArrayList;

public class UMLDiagram {
    public class K{

    }

    public class L extends K{ // INHERITANCE
        private int a;
        private M m; // ASSOCIATION
        public void b(){}
        public void i(){}
    }

    public class X{
        public void met(L l){} // DEPENDENCY
    }

    public class M{
        public ArrayList<B> bs = new ArrayList<>(); // COMPOSITION - I considered multiple objects not just 1
        M(){
            for(B b:bs)
                b = new B();
        }
    }

    public class A{
        private ArrayList<M> as = new ArrayList<>(); // AGGREGATION
    }

    public class B{

    }
}
